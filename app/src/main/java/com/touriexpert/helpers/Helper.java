package com.touriexpert.helpers;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.touriexpert.main.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Helper {

    private static final String TAG = "Helper";

    public static String getConfigValue(Context context, String name) {
        Resources resources = context.getResources();

        try {
            InputStream rawResource = resources.openRawResource(R.raw.config);
            Properties properties = new Properties();
            properties.load(rawResource);
            return properties.getProperty(name);
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Impossibile trovare il file di configurazione: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "Apertura del file di configurazione fallita.");
        }

        return null;
    }
}
