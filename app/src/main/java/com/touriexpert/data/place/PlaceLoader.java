package com.touriexpert.data.place;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.touriexpert.main.R;
import com.touriexpert.models.place.Place;
import com.touriexpert.network.PlaceService;

import java.net.ConnectException;

/**
 * Implemetazione custom della classe del framework {@link android.content.CursorLoader}
 */
public final class PlaceLoader extends AsyncTaskLoader<Place> {
    private static final String TAG = "PlaceLoader";

    private PlaceService placeService;
    private Resources resources;
    private OnErrorListener onErrorListener;
    private Place place;

    public interface OnErrorListener {
        void onLoaderError(Exception error);
    }

    public PlaceLoader(Context context) {
        super(context);
        this.placeService = PlaceService.getInstance(context);
        this.resources = context.getResources();
    }

    public OnErrorListener getOnErrorListener() {
        return onErrorListener;
    }

    public void setOnErrorListener(OnErrorListener onErrorListener) {
        this.onErrorListener = onErrorListener;
    }

    /* Runs on a worker thread */
    @Override
    public Place loadInBackground() {
        Log.d(TAG, "--> PlaceLoader Started");
        try {
            this.place = this.placeService.getPlace();
        } catch (ConnectException e) {
            Log.e(TAG, this.resources.getString(R.string.menuLoadInBackgroundExceptionMsg) + e.getMessage());
            this.onErrorListener.onLoaderError(e);
            //throw new RuntimeException(e);
        }
        return this.place;
    }

    /**
     * Starts an asynchronous load of the contacts list data. When the result is ready the callbacks
     * will be called on the UI thread. If a previous load has been completed and is still valid
     * the result may be passed to the callbacks immediately.
     *
     * Must be called from the UI thread
     */
    @Override
    protected void onStartLoading() {
        if (this.place != null) {
            deliverResult(this.place);
        }
        if (takeContentChanged() || this.place == null) {
            forceLoad();
        }
    }

    /**
     * Must be called from the UI thread
     */
    @Override
    protected void onStopLoading() {
        // Attempt to cancel the current load task if possible.
        cancelLoad();
    }
}