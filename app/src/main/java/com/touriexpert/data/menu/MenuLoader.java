package com.touriexpert.data.menu;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.os.CancellationSignal;
import android.support.v4.os.OperationCanceledException;
import android.util.Log;

import com.touriexpert.main.R;
import com.touriexpert.models.ristorazione.Menu;
import com.touriexpert.network.RistService;

import java.net.ConnectException;

/**
 * Implemetazione custom della classe del framework {@link android.content.CursorLoader}
 */
public final class MenuLoader extends AsyncTaskLoader<Menu> {
    private static final String TAG = "PlaceLoader";

    private Menu ristMenu;
    private CancellationSignal mCancellationSignal;
    private RistService ristService;
    private Resources resources;
    private OnErrorListener onErrorListener;

    public interface OnErrorListener {
        void onLoaderError(Exception error);
    }

    public MenuLoader(Context context) {
        super(context);
        this.ristService = RistService.getInstance(context);
        this.resources = context.getResources();
    }

    public OnErrorListener getOnErrorListener() {
        return onErrorListener;
    }

    public void setOnErrorListener(OnErrorListener onErrorListener) {
        this.onErrorListener = onErrorListener;
    }

    @Override
    public Menu loadInBackground() {
        Menu ristMenu = null;
        Log.d(TAG, "--> PlaceLoader Started");

        try {
            ristMenu = this.ristService.getMenu();
        } catch (ConnectException e) {
            Log.e(TAG, this.resources.getString(R.string.menuLoadInBackgroundExceptionMsg) + e.getMessage());
            throw new RuntimeException(e);
        }

        return ristMenu;
    }

    @Override
    protected void onStartLoading() {
        if (takeContentChanged())
            forceLoad();
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }
}