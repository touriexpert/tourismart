package com.touriexpert.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.touriexpert.main.R;

public class CounterFragment extends Fragment {
    private TextView txtCounterNumber;
    private TextView txtCounterTitle;
    private TextView txtCounterQst;
    private View root;
    private Integer minCount;
    private Integer maxCount;

    
    public void setCounterNumber(int number){
        this.txtCounterNumber.setText(Integer.toString(number));
    }

    public int getCounterNumber(){
        return Integer.parseInt(this.txtCounterNumber.getText().toString());
    }

    public void setCounterTitle(int titleID){
        this.txtCounterTitle.setText(titleID);
    }

    public void setCounterQuestion(int questionID){
        this.txtCounterQst.setText(questionID);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.counter, container, false);
        this.txtCounterNumber = (TextView)root.findViewById(R.id.counterNumber);
        this.txtCounterTitle = (TextView)this.root.findViewById(R.id.txtCounterTitle);
        this.txtCounterQst = (TextView)this.root.findViewById(R.id.txtCounterQst);
        Button btnIncPeople = (Button)root.findViewById(R.id.btnIncCounter);
        Button btnDecPeople = (Button)root.findViewById(R.id.btnDecCounter);
        btnIncPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPeopleNumber("inc");
            }
        });
        btnDecPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPeopleNumber("dec");
            }
        });
        return root;
    }

    public void setMinCount(int minCount){
        this.minCount = minCount;
        this.txtCounterNumber.setText(Integer.toString(this.minCount));
    }

    public void setMaxCount(int maxCount){
        this.maxCount = maxCount;
    }

    private void setPeopleNumber(String op) {
        String txtValue = txtCounterNumber.getText().toString();
        try {
            int peopleNumber = Integer.parseInt(txtValue);
            if(op.equals("dec") && peopleNumber > 0){
                if(this.minCount != null && peopleNumber <= this.minCount){
                    peopleNumber = this.minCount;
                } else {
                    peopleNumber--;
                }
            } else if(op.equals("inc")){
                if(this.maxCount != null && peopleNumber >= this.maxCount){
                    peopleNumber = this.maxCount;
                } else {
                    peopleNumber++;
                }
            }
            this.txtCounterNumber.setText(Integer.toString(peopleNumber));
        } catch (NumberFormatException e){
            this.txtCounterNumber.setText(Integer.toString(0));
        }
    }
}
