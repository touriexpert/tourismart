package com.touriexpert.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.touriexpert.adapters.menu.MenuBuildListAdapter;
import com.touriexpert.main.R;

public class BuildFragment extends Fragment implements MenuBuildListAdapter.ItemAddedListener{
    private MenuBuildListAdapter listAdapter;
    private RecyclerView buildList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        this.listAdapter = (MenuBuildListAdapter)bundle.getSerializable("menuAdapter");
        this.listAdapter.setItemAddedListener(this);
        View rootView = inflater.inflate(R.layout.place_build_fragment_list, container, false);
        this.buildList = (RecyclerView)rootView.findViewById(R.id.list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.buildList.getContext());
        this.buildList.setLayoutManager(layoutManager);
        this.buildList.setItemAnimator(new DefaultItemAnimator());
        this.buildList.setAdapter(listAdapter);
        return rootView;
    }

    @Override
    public void onItemAdded() {
        this.buildList.scrollToPosition(0);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
