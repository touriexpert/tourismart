package com.touriexpert.network;

import android.content.Context;

import com.google.gson.JsonSyntaxException;
import com.touriexpert.helpers.Helper;
import com.touriexpert.models.ristorazione.Articoli;
import com.touriexpert.models.ristorazione.Articolo;
import com.touriexpert.models.ristorazione.Menu;
import com.touriexpert.models.ristorazione.MenuItemModel;
import com.touriexpert.models.ristorazione.ReservationModel;
import com.touriexpert.models.UserModel;
import com.touriexpert.models.ristorazione.RistPayment;

import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.transport.HttpResponseException;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.net.ConnectException;
import java.util.List;
import java.util.Map;

public final class RistService extends PlaceService {
    private static RistService singleton;
    private SoapObject ristRequest;
    private String sessionCode;

    private RistService(Context context) {
        super(context);
        this.sessionCode = UserModel.getInstance().getSessionCode();
    }

    public static RistService getInstance(Context context){
        if(RistService.singleton == null){
            RistService.singleton = new RistService(context);
        }
        return new RistService(context);
    }

    public Integer getRistTables(){
        String availableTables = Helper.getConfigValue(this.context, "availableTablesMethod");
        this.ristRequest = new SoapObject(NAMESPACE, availableTables);
        this.envelope.setOutputSoapObject(this.ristRequest);
        String response;
        Integer tables = -1;
        try {
            this.ht.call("", this.envelope);
            response = this.envelope.getResponse().toString();
            tables = this.gson.fromJson(response, Integer.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tables;
    }

    private void setMenu(Map menuMap, Menu menu){
        menu.setCategorie((List<String>) menuMap.get("categorie"));
        Double menuID = (Double) menuMap.get("id");
        menu.setId(menuID.intValue());
        Articoli articoli = menu.getArticoli();
        Articolo articolo = null;
        Map articoliMap = (Map) menuMap.get("articoli");
        for(Object key : articoliMap.keySet()){
            List<Object> articoliList = (List<Object>) articoliMap.get(key);
            for(Object art : articoliList){
                Map articoloMap = (Map) art;
                articolo = new Articolo();
                articolo.setId(((Double)articoloMap.get("id")).intValue());
                articolo.setNome((String)articoloMap.get("nome"));
                articolo.setDescrizione((String)articoloMap.get("descrizione"));
                Double prezzo = Double.parseDouble((String) articoloMap.get("prezzo"));
                articolo.setPrezzo(prezzo);
                articolo.addCategoria((String)key);
                articoli.addArticolo((String)key, articolo);
            }
        }
    }

    public Menu getMenu() throws ConnectException {
        String getMenuMethod = Helper.getConfigValue(this.context, "getMenuMethod");
        Map test = null;
        Menu menu = new Menu();
        String placeID = this.getPlace().getId().toString();
        this.ristRequest = new SoapObject(NAMESPACE, getMenuMethod);
        this.ristRequest.addProperty("placeID", placeID);
        this.ristRequest.addProperty("reservationDate", "");
        this.ristRequest.addProperty("reservationTime", "");
        this.ristRequest.addProperty("sessionCode", this.sessionCode);
        this.envelope.setOutputSoapObject(this.ristRequest);
        String response;
        try {
            this.ht.call("", this.envelope);
            response = this.envelope.getResponse().toString();
            test = this.gson.fromJson(response, Map.class);
            setMenu(test, menu);
        } catch (SoapFault e) {
            throw new ConnectException(e.getMessage());
        } catch (HttpResponseException e) {
            throw new ConnectException(e.getMessage());
        } catch (XmlPullParserException e) {
            throw new ConnectException(e.getMessage());
        } catch (IOException e) {
            throw new ConnectException(e.getMessage());
        } catch (JsonSyntaxException e){
            throw new ConnectException(e.getMessage());
        }
        return menu;
    }

    public boolean sendMenuItem(MenuItemModel menuItem){
        return false;
    }

    public RistPayment sendReservation(ReservationModel reservModel, List<MenuItemModel> menus,
                                       String sessionCode) throws ConnectException {
        String reservModelJson = gson.toJson(reservModel);
        String menusJson = gson.toJson(menus);
        String placeID = this.getPlace().getId().toString();
        RistPayment ristPayment = new RistPayment();
        this.ristRequest = new SoapObject(NAMESPACE, "sendReservation");
        this.ristRequest.addProperty("placeID", placeID);
        this.ristRequest.addProperty("reservationData", reservModelJson);
        this.ristRequest.addProperty("ordinazioni", menusJson);
        this.ristRequest.addProperty("sessionCode", sessionCode);
        this.envelope.setOutputSoapObject(this.ristRequest);
        String response;
        try {
            this.ht.call("", this.envelope);
            response = this.envelope.getResponse().toString();
            ristPayment = this.gson.fromJson(response, ristPayment.getClass());
        } catch (Exception e) {
            throw new ConnectException(e.getMessage());
        }
        return ristPayment;
    }

    public boolean sendPaymentProof(String paymentCode, int idComanda, int idPrenotazione)
            throws ConnectException {
        boolean isPayed = false;
        this.ristRequest = new SoapObject(NAMESPACE, "savePaymentProof");
        this.ristRequest.addProperty("idComanda", idComanda);
        this.ristRequest.addProperty("idPrenotazione", idPrenotazione);
        this.ristRequest.addProperty("paymentCode", paymentCode);
        this.ristRequest.addProperty("sessionCode", this.sessionCode);
        this.envelope.setOutputSoapObject(this.ristRequest);
        String response;
        try {
            this.ht.call("", this.envelope);
            response = this.envelope.getResponse().toString();
            isPayed = this.gson.fromJson(response, Boolean.class);
        } catch (HttpResponseException e) {
            throw new ConnectException(e.getMessage());
        } catch (SoapFault e) {
            throw new ConnectException(e.getMessage());
        } catch (XmlPullParserException e) {
            throw new ConnectException(e.getMessage());
        } catch (IOException e) {
            throw new ConnectException(e.getMessage());
        }
        return isPayed;
    }
}
