package com.touriexpert.network;

import android.content.Context;

import com.google.gson.Gson;
import com.touriexpert.helpers.Helper;
import com.touriexpert.models.place.Place;

import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.net.ConnectException;
import java.util.ArrayList;

public class PlaceService {
    private static Place mainPlace;
    private static PlaceService singleton;

    protected static String NAMESPACE;
    protected static String URL;

    protected Gson gson = new Gson();
    protected HttpTransportSE ht;
    protected SoapSerializationEnvelope envelope;
    protected ArrayList<HeaderProperty> headerPropertyArrayList;
    protected Context context;
    private SoapObject request;

    protected PlaceService(Context context){
        this.context = context;
        NAMESPACE = Helper.getConfigValue(this.context, "apiNamespace");
        URL = Helper.getConfigValue(this.context, "apiUrl");
        this.envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        this.ht = new HttpTransportSE(URL, 60000);
        this.headerPropertyArrayList = new ArrayList<>();
        this.headerPropertyArrayList.add(new HeaderProperty("Connection", "close"));
    }

    public static PlaceService getInstance(Context context){
        if(PlaceService.singleton == null){
            PlaceService.singleton = new PlaceService(context);
        }
        return PlaceService.singleton;
    }

    public Place getPlace() throws ConnectException {
        if(mainPlace == null) {
            String placeInfoMethod = Helper.getConfigValue(this.context, "placeInfoMethod");
            String placeIdParam = Helper.getConfigValue(this.context, "placeIdParam");
            this.request = new SoapObject(NAMESPACE, placeInfoMethod);
            this.request.addProperty(placeIdParam, "2");
            this.envelope.setOutputSoapObject(this.request);
            String response;
            try {
                this.ht.call("", this.envelope, this.headerPropertyArrayList);
                response = this.envelope.getResponse().toString();
                mainPlace = this.gson.fromJson(response, Place.class);
            } catch (ConnectException e) {
                throw e;
            } catch (Exception e) {
                throw new ConnectException(e.getMessage());
            }
        }
        return mainPlace;
    }
}
