package com.touriexpert.activities.intrattenimento;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.touriexpert.main.R;
import com.touriexpert.activities.common.MainActivity;

public class IntrMainActivity extends MainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initializeView(R.color.intr_yellow);
        this.setInfoButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IntrMainActivity.this, IntrInfoActivity.class);
                IntrMainActivity.this.startActivity(intent);
            }
        });
    }

}
