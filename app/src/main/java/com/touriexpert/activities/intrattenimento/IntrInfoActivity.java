package com.touriexpert.activities.intrattenimento;

import android.os.Bundle;

import com.touriexpert.main.R;
import com.touriexpert.activities.common.InfoActivity;

public class IntrInfoActivity extends InfoActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initializeView(R.color.intr_yellow);
    }
}
