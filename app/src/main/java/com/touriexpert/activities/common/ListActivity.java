package com.touriexpert.activities.common;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.touriexpert.main.R;

public class ListActivity extends TouriSmartActivity {
    private int idMainContainer = R.id.mainContainer;
    private int idMainButton = R.id.btnMainButton;
    private int idMainList = R.id.mainList;
    private int idEmptyMessage = R.id.emptyMessage;
    private int idAddButton = R.id.addButton;
    private int idToolbar = R.id.listToolbar;
    private Integer mainColor = null;

    @Override
    protected void initializeView(int mainColor) {

    }

    protected void setToolbar(int backgroundColor, int title){
        this.setToolbar(this.idToolbar, backgroundColor, title);
    }

    protected void setSkipButton(int labelID, View.OnClickListener listener){
        this.setButton(this.idMainButton, labelID, listener, this.mainColor);
    }

    protected void setConfirmButton(int labelID, View.OnClickListener listener){
        this.setButton(this.idMainButton, labelID, listener, this.mainColor);
    }

    protected void updateConfirmButton(int labelID){
        this.updateButtonLabel(this.idMainButton, labelID);
    }

    protected void setAddButton(View.OnClickListener listener){
        this.setFabButton(this.idAddButton, listener);
    }

    protected void showAddButton(){
        this.showView(this.idAddButton);
    }

    protected void setEmptyMessage(int message){
        TextView emptyMessage = (TextView)this.findViewById(this.idEmptyMessage);
        emptyMessage.setText(message);
    }

    protected void hideEmptyMessage(){
        this.hideView(this.idEmptyMessage);
    }

    protected void showEmptyMessage(){
        this.showView(this.idEmptyMessage);
    }

    protected void setList(RecyclerView.Adapter adapter) {
        RecyclerView mainList = (RecyclerView) this.findViewById(this.idMainList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mainList.setLayoutManager(layoutManager);
        DefaultItemAnimator animator = new DefaultItemAnimator();
        animator.setAddDuration(2000);
        animator.setRemoveDuration(2000);
        mainList.setItemAnimator(animator);
        mainList.setAdapter(adapter);
    }
}
