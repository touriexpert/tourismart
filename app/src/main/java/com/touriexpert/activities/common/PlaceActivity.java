package com.touriexpert.activities.common;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;

import com.touriexpert.data.place.PlaceLoader;
import com.touriexpert.models.place.Place;

public abstract class PlaceActivity extends TouriSmartActivity
        implements LoaderManager.LoaderCallbacks<Place>, PlaceLoader.OnErrorListener {

    private static final String TAG = "PlaceActivity";
    private int mainColor;

    @Override
    public Loader<Place> onCreateLoader(int i, Bundle bundle) {
        PlaceLoader loader = new PlaceLoader(this);
        loader.setOnErrorListener(this);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Place> loader, Place place) {
        Log.e(TAG, "Metodo: onLoadFinished. Effettuare l'override");
        throw new RuntimeException("Stub!");
    }

    @Override
    public void onLoaderReset(Loader<Place> loader) {

    }

    @Override
    protected void initializeView(int mainColor) {
        this.mainColor = mainColor;
    }

    @Override
    public void onLoaderError(Exception error) {
        Intent intent = new Intent(this, NoConnectionActivity.class);
        intent.putExtra("mainColor", this.mainColor);
        this.startActivityForResult(intent, CONNECT_AGAIN_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CONNECT_AGAIN_REQUEST) {
            if (resultCode == Activity.RESULT_OK){
                //this.getLoaderManager().getLoader(MAIN_LOADER_ID).onContentChanged();
                this.getLoaderManager().getLoader(this.getLoaderId()).forceLoad();
            }
        }
    }

    protected abstract int getLoaderId();
}
