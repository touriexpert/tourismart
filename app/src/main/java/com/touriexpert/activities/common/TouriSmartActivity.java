package com.touriexpert.activities.common;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.touriexpert.main.R;

public abstract class TouriSmartActivity extends AppCompatActivity {
    protected static final int CONNECT_AGAIN_REQUEST = 1;

    protected void setBackground(int color, int containerID){
        ViewGroup main = (ViewGroup)this.findViewById(containerID);
        main.setBackgroundColor(getResources().getColor(color));
    }

    protected void setToolbar(int toolbarID, int backgroundColor, Integer title){
        Toolbar mainToolbar = (Toolbar) findViewById(toolbarID);
        setSupportActionBar(mainToolbar);
        String toolbarColor = getResources().getString(backgroundColor);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor(toolbarColor)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_white_24dp);
        if(title != null){
            getSupportActionBar().setTitle(getResources().getString(title));
        }
    }

    protected void removeView(int containerID, View view){
        ViewGroup container = (ViewGroup)this.findViewById(containerID);
        container.removeView(view);
    }

    protected void hideView(int viewID){
        View view = (View)this.findViewById(viewID);
        view.setVisibility(View.INVISIBLE);
    }

    protected void showView(int viewID){
        View view = (View)this.findViewById(viewID);
        view.setVisibility(View.VISIBLE);
    }

    protected void setButton(int buttonID, Integer labelID, View.OnClickListener listener, Integer textColor){
        this.setPButton(buttonID, labelID, listener, textColor);
    }

    protected void setButton(int buttonID, Integer labelID, View.OnClickListener listener){
        this.setPButton(buttonID, labelID, listener, null);
    }

    protected void setButton(int buttonID, View.OnClickListener listener){
        this.setPButton(buttonID, null, listener, null);
    }

    private void setPButton(int buttonID, Integer labelID, View.OnClickListener listener, Integer color){
        Button button = (Button)this.findViewById(buttonID);
        button.setOnClickListener(listener);
        if(labelID != null) {
            button.setText(labelID);
        }
        if(color != null){
            button.setTextColor(getResources().getColor(color));
        }
    }

    protected void setFabButton(int buttonID, View.OnClickListener listener){
        FloatingActionButton addButton = (FloatingActionButton)this.findViewById(buttonID);
        addButton.setOnClickListener(listener);
    }

    protected void updateButtonLabel(int buttonID, Integer labelID){
        Button button = (Button)this.findViewById(buttonID);
        button.setText(labelID);
    }

    protected void setTextView(int textViewID, String text){
        TextView textView = (TextView)this.findViewById(textViewID);
        textView.setText(text);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return false;
        }
    }

    protected abstract void initializeView(int mainColor);
}