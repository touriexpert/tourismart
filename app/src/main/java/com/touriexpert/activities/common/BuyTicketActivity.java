package com.touriexpert.activities.common;

import android.widget.TextView;

import com.touriexpert.main.R;

public class BuyTicketActivity extends TouriSmartActivity {
    private int idMainLayout = R.layout.buy_ticket;
    private int idMainContainer = R.id.mainContainer;
    private int idPlaceName = R.id.txtPlaceName;

    protected void initializeView(int mainColor){
        this.setContentView(this.idMainLayout);
        this.setBackground(mainColor, this.idMainContainer);
    }

    protected void setPlaceName(int placeName){
        TextView txtPlaceName = (TextView)this.findViewById(this.idPlaceName);
        txtPlaceName.setText(placeName);
    }
}
