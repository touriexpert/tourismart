package com.touriexpert.activities.common;

import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.touriexpert.main.R;
import com.touriexpert.fragments.ReviewListAdapter;

public class ReviewsActivity extends PlaceActivity {
    private static final int REVIEWS_LOADER_ID = 4;

    private int idMainLayout = R.layout.place_reviews;
    private int idListReview = R.id.reviewsList;

    private String[] values = new String[] {
            "Bigoli in Salsa",
            "Capesante arrostite",
            "Risotto mantecato",
            "Baccalà in dolce",
            "Filetto di vitello in crosta",
            "Cappelletti in brodo",
            "Tonno foie gras e mela",
            "Astice e vitello",
            "Spalla di agnello iberico",
            "Panna cotta alla vaniglia"
    };

    @Override
    protected void initializeView(int mainColor) {
        setContentView(this.idMainLayout);
        this.setToolbar(R.id.reviewsToolbar, mainColor, R.string.rist_reviews_list_title);
    }

    @Override
    protected int getLoaderId() {
        return REVIEWS_LOADER_ID;
    }

    protected void setAddReviewsButton(View.OnClickListener listener){
        FloatingActionButton fabAddMenu = (FloatingActionButton)this.findViewById(R.id.addButton);
        fabAddMenu.setOnClickListener(listener);
    }

    protected void setReviewsList(ListAdapter adapter){
        ListView menuBuildList = (ListView)this.findViewById(this.idListReview);
        menuBuildList.setAdapter(adapter);
    }

    protected void setDefaultAdapter(){
        ListView reviewsList = (ListView)this.findViewById(R.id.reviewsList);
        ReviewListAdapter adapter = new ReviewListAdapter(this, this.values);
        reviewsList.setAdapter(adapter);
    }
}
