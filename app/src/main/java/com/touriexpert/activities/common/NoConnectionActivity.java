package com.touriexpert.activities.common;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.touriexpert.main.R;

public class NoConnectionActivity extends TouriSmartActivity {
    private int idMainLayout = R.layout.no_connection;
    private int idMainContainer = R.id.mainContainer;
    private int idMainButton = R.id.btnMain;
    private int mainColor = R.color.black;
    private int loaderID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(this.idMainLayout);
        Intent mainIntent = getIntent();
        if(mainIntent != null){
            this.mainColor = mainIntent.getIntExtra("mainColor", R.color.black);
        }
        setBackground(this.mainColor, this.idMainContainer);
        Button mainButton = (Button) this.findViewById(idMainButton);
        mainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NoConnectionActivity.this.setResult(NoConnectionActivity.RESULT_OK);
                finish();
            }
        });
    }

    @Override
    protected void initializeView(int mainColor) {

    }

}
