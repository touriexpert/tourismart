package com.touriexpert.activities.common;

import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.touriexpert.main.R;
import com.touriexpert.models.place.Place;

import java.util.Map;

public class InfoActivity extends PlaceActivity {
    private static final int INFO_LOADER_ID = 3;
    private static final String TAG = "InfoActivity";

    private int idMainLayout = R.layout.place_info;
    private int idMainContainer = R.id.mainContainer;
    private int idHoursContainer = R.id.hoursContainer;
    private int idPlaceName = R.id.placeName;
    private int idPlaceAddress = R.id.placeAddress;
    private int idPlaceCall = R.id.btnCall;
    private int idPlaceWebsite = R.id.btnWebsite;
    private LayoutInflater inflater;
    private FrameLayout hoursMain;

    @Override
    protected void initializeView(int mainColor) {
        super.initializeView(mainColor);
        setContentView(this.idMainLayout);
        this.setBackground(mainColor, this.idMainContainer);
        this.inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.hoursMain = (FrameLayout)this.findViewById(R.id.hoursMain);
        int backgroundColor = getResources().getColor(mainColor);
        LinearLayout main = (LinearLayout)this.findViewById(idMainContainer);
        main.setBackgroundColor(backgroundColor);
        LinearLayout roundWrapper = (LinearLayout)this.findViewById(R.id.roundWrapper);
        roundWrapper.setBackgroundColor(backgroundColor);
        getLoaderManager().initLoader(INFO_LOADER_ID, null, this);
    }

    private void addOpening(String day, String openTime, String closeTime) {
        LinearLayout openingContainer = (LinearLayout)this.findViewById(idHoursContainer);
        LinearLayout openingsItem = (LinearLayout)inflater.inflate(R.layout.place_openings, null);
        TextView txtDay = (TextView)openingsItem.findViewById(R.id.txtDay);
        TextView txtOpenTime = (TextView)openingsItem.findViewById(R.id.txtOpenTime);
        TextView txtCloseTime = (TextView)openingsItem.findViewById(R.id.txtCloseTime);
        txtDay.setText(day);
        txtOpenTime.setText(openTime);
        txtCloseTime.setText(closeTime);
        openingContainer.addView(openingsItem);
    }

    private void setInfoButton(int buttonID, final String action, final String actionData){
        Button btnWebsite = (Button)findViewById(buttonID);
        if(btnWebsite != null) {
            btnWebsite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent dialIntent = new Intent(action);
                    dialIntent.setData(Uri.parse(actionData));
                    startActivity(dialIntent);
                    }
            });
        } else {
            Log.e(TAG, "btnWebsite is null");
        }
    }

    private void setButtonCall(final String number){
        this.setInfoButton(this.idPlaceCall, Intent.ACTION_DIAL, number);
    }

    private void setButtonWebsite(final String website){
        this.setInfoButton(this.idPlaceWebsite, Intent.ACTION_VIEW, website);
    }

    @Override
    public void onLoadFinished(Loader<Place> loader, Place place) {
        this.setTextView(this.idPlaceName, place.getNome());
        this.setTextView(this.idPlaceAddress, place.getIndirizzo());
        Map<String, String[]> orari = place.getOrari();
        if (orari != null) {
            for (String day : orari.keySet()) {
                String openTime = orari.get(day)[0];
                String closeTime = orari.get(day)[1];
                addOpening(day, openTime, closeTime);
            }
        } else {
            hoursMain.setVisibility(View.INVISIBLE);
        }
        setButtonCall(place.getTelefono());
        setButtonWebsite(place.getWebsite());
    }

    @Override
    protected int getLoaderId() {
        return INFO_LOADER_ID;
    }
}
