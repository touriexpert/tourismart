package com.touriexpert.activities.common;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import com.touriexpert.main.R;

public class BuildActivity extends TouriSmartActivity {
    private int idMainLayout = R.layout.place_build;
    private int idMainContainer = R.id.mainContainer;
    private int idButtonSave = R.id.btnSave;
    private int idToolbar = R.id.buildToolbar;
    private TabLayout tabLayout;
    private ViewPager menuPager;

    @Override
    protected void initializeView(int mainColor) {
        this.setContentView(this.idMainLayout);
        this.setBackground(mainColor, this.idMainContainer);
        this.menuPager = (ViewPager)findViewById(R.id.menuPager);
        this.tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
    }

    protected void setPagerAdapter(FragmentPagerAdapter adapter){
        this.menuPager.setAdapter(adapter);
        this.tabLayout.setupWithViewPager(this.menuPager);
    }

    protected void setButtonSave(View.OnClickListener listener){
        Button saveButton = (Button)this.findViewById(this.idButtonSave);
        saveButton.setOnClickListener(listener);
    }

    protected void setToolbar(int backgroundColor, int title){
        this.setToolbar(this.idToolbar, backgroundColor, title);
    }

}
