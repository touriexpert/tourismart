package com.touriexpert.activities.common;

import android.content.Loader;
import android.view.View;

import com.touriexpert.main.R;
import com.touriexpert.models.place.Place;

public class MainActivity extends PlaceActivity
{
    private static final int MAIN_LOADER_ID = 2;

    private int idMainLayout = R.layout.place_main;
    private int idPlaceMainButton = R.id.placeMainButton;
    private int idPlaceReviewsButton = R.id.buttonReviews;
    private int idPlaceInfoButton = R.id.buttonInfo;
    private int idMainContainer = R.id.mainContainer;
    private int idPlaceName = R.id.placeName;
    private int idPlaceAddress = R.id.placeAddress;
    private int mainColor;

    protected void initializeView(int mainColor){
        super.initializeView(mainColor);
        this.mainColor = mainColor;
        setContentView(this.idMainLayout);
        setBackground(mainColor, this.idMainContainer);
        getLoaderManager().initLoader(MAIN_LOADER_ID, null, this);
    }

    protected void setMainButton(int labelID, View.OnClickListener listener){
        this.setButton(this.idPlaceMainButton, labelID, listener, this.mainColor);
    }

    protected void setReviewsButton(View.OnClickListener listener){
        this.setButton(this.idPlaceReviewsButton, listener);
    }

    protected void setInfoButton(View.OnClickListener listener){
        this.setButton(this.idPlaceInfoButton, listener);
    }

    @Override
    public void onLoadFinished(Loader<Place> loader, Place place) {
        if(place != null) {
            setTextView(idPlaceName, place.getNome());
            setTextView(idPlaceAddress, place.getIndirizzo());
        }
    }

    @Override
    protected int getLoaderId() {
        return MAIN_LOADER_ID;
    }

}
