package com.touriexpert.activities.cultura.path.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.touriexpert.main.R;
import com.touriexpert.activities.cultura.path.models.PathItemModel;

import java.util.ArrayList;
import java.util.List;

public class PathListAdapter extends RecyclerView.Adapter<PathListAdapter.PathItemHolder> {

    class PathItemHolder extends RecyclerView.ViewHolder {
        TextView txtPathOwnerName, txtPathPrice;
        Button btnDeletePath;

        PathItemHolder(View view) {
            super(view);
            this.txtPathOwnerName = (TextView) view.findViewById(R.id.txtPathOwnerName);
            this.btnDeletePath = (Button) view.findViewById(R.id.btnDeletePath);
        }
    }

    private List<PathItemModel> pathList;

    public PathListAdapter(){
        this.pathList = new ArrayList<PathItemModel>();
    }

    @Override
    public PathItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View rootView = inflater.inflate(R.layout.cult_path_list_item, parent, false);
        return new PathItemHolder(rootView);
    }

    @Override
    public void onBindViewHolder(PathItemHolder holder, int position) {
        int pos = position;
        PathItemModel menuModel = this.pathList.get(position);
        holder.txtPathOwnerName.setText(menuModel.getOwner());
        holder.btnDeletePath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.pathList.size();
    }
}
