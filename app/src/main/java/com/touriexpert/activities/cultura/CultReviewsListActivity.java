package com.touriexpert.activities.cultura;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.touriexpert.activities.common.ReviewsActivity;
import com.touriexpert.main.R;
import com.touriexpert.activities.ristorazione.RistWriteReviewActivity;


public class CultReviewsListActivity extends ReviewsActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initializeView(R.color.cult_blu);
        this.setAddReviewsButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CultReviewsListActivity.this, RistWriteReviewActivity.class);
                CultReviewsListActivity.this.startActivity(intent);
            }
        });
        this.setDefaultAdapter();
    }
}