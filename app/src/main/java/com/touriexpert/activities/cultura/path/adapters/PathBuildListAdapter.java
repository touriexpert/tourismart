package com.touriexpert.activities.cultura.path.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.touriexpert.main.R;
import com.touriexpert.activities.cultura.path.models.PathBuildItemModel;

import java.util.ArrayList;
import java.util.List;

public class PathBuildListAdapter extends RecyclerView.Adapter<PathBuildListAdapter.BuildItemHolder>{

    class BuildItemHolder extends RecyclerView.ViewHolder {
        TextView txtItemName, txtItemDescription;
        Button btnAddItem;

        BuildItemHolder(View view) {
            super(view);
            this.txtItemName = (TextView) view.findViewById(R.id.txtItemName);
            this.txtItemDescription = (TextView) view.findViewById(R.id.txtItemDescription);
            this.btnAddItem = (Button) view.findViewById(R.id.btnAddItem);
        }
    }

    private List<PathBuildItemModel> buildList;

    private String[] values = new String[] {
            "Masaccio – Il pagamento del tributo",
            "Piero della Francesca – Flagellazione di Cristo",
            "Jean Fouquet – Madonna (particolare de Il dittico di Melun)",
            "Andrea Mantegna – Cristo morto",
            "Sandro Botticelli – La nascita di Venere",
            "Leonardo da Vinci – L’ultima cena",
            "Leonardo da Vinci – La Gioconda",
            "Raffaello Sanzio – La scuola d Atene",
            "Michelangelo Buonarroti – La cappella sistina",
            "Caravaggio – Medusa"
    };

    public PathBuildListAdapter() {
        this.buildList = new ArrayList<PathBuildItemModel>();
        for(int i = 0; i < values.length; i++){
            PathBuildItemModel item = new PathBuildItemModel();
            item.setItemName(this.values[i]);
            this.buildList.add(item);
        }
    }

    @Override
    public BuildItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cult_build_list_item, parent, false);
        return new BuildItemHolder(rootView);
    }

    @Override
    public void onBindViewHolder(BuildItemHolder holder, int position) {
        int pos = position;
        PathBuildItemModel menuModel = this.buildList.get(position);
        holder.txtItemName.setText(menuModel.getItemName());
        holder.txtItemDescription.setText(menuModel.getItemDescription());
        holder.btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.buildList.size();
    }

}
