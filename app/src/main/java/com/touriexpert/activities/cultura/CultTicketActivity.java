package com.touriexpert.activities.cultura;

import android.os.Bundle;

import com.touriexpert.main.R;
import com.touriexpert.activities.common.BuyTicketActivity;
import com.touriexpert.fragments.CounterFragment;

public class CultTicketActivity extends BuyTicketActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initializeView(R.color.cult_blu);
        CounterFragment counter = (CounterFragment)this.getSupportFragmentManager()
                .findFragmentById(R.id.ticketCounter);
        counter.setCounterTitle(R.string.cult_tickets_number);
        counter.setCounterQuestion(R.string.cult_tickets_number_qst);
    }
}
