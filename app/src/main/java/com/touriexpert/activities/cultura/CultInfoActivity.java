package com.touriexpert.activities.cultura;

import android.os.Bundle;

import com.touriexpert.main.R;
import com.touriexpert.activities.common.InfoActivity;

public class CultInfoActivity extends InfoActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initializeView(R.color.cult_blu);
    }
}
