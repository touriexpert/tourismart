package com.touriexpert.activities.cultura;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.touriexpert.main.R;
import com.touriexpert.activities.common.ListActivity;
import com.touriexpert.activities.cultura.path.adapters.PathListAdapter;

public class CultPathListActivity extends ListActivity{
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cult_build_list);
        this.initializeView(R.color.cult_blu);
        this.setToolbar(R.color.cult_blu_dark, R.string.cult_path_list);
        this.setList(new PathListAdapter());
        this.setEmptyMessage(R.string.cult_no_path);
        this.setSkipButton(R.string.cult_skip, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CultPathListActivity.this, CultTicketActivity.class);
                CultPathListActivity.this.startActivity(intent);
            }
        });
        this.setAddButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CultPathListActivity.this, CultPathBuildActivity.class);
                CultPathListActivity.this.startActivity(intent);
            }
        });
    }
}
