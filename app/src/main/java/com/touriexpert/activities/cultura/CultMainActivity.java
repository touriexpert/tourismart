package com.touriexpert.activities.cultura;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.touriexpert.main.R;
import com.touriexpert.activities.common.MainActivity;

public class CultMainActivity extends MainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initializeView(R.color.cult_blu);
        this.setMainButton(R.string.cult_path,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(CultMainActivity.this, CultPathListActivity.class);
                        CultMainActivity.this.startActivity(intent);
                    }
                }
        );
        this.setReviewsButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CultMainActivity.this, CultReviewsListActivity.class);
                CultMainActivity.this.startActivity(intent);
            }
        });
        this.setInfoButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CultMainActivity.this, CultInfoActivity.class);
                CultMainActivity.this.startActivity(intent);
            }
        });
    }

}
