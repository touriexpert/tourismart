package com.touriexpert.activities.cultura.path.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PathBuildPageAdapter extends FragmentPagerAdapter {
    private String[] categorie = {
            "Quadri",
            "Sculture",
            "Installazioni",
            "Proiezioni",
            "Mostre",
            "Modellini"
    };
    private FragmentManager fragmentManager;
    private Context context;

    public PathBuildPageAdapter(FragmentManager fm, Context context){
        super(fm);
        this.fragmentManager = fm;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return categorie[position];
    }
}
