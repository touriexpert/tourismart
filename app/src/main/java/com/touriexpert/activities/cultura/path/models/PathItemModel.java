package com.touriexpert.activities.cultura.path.models;

public class PathItemModel {
    private String owner;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
