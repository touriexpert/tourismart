package com.touriexpert.activities.cultura;

import android.os.Bundle;

import com.touriexpert.main.R;
import com.touriexpert.activities.common.BuildActivity;
import com.touriexpert.activities.cultura.path.adapters.PathBuildPageAdapter;

public class CultPathBuildActivity extends BuildActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initializeView(R.color.cult_blu);
        this.setToolbar(R.color.cult_blu_dark, R.string.cult_path);
        PathBuildPageAdapter menuBuildPageAdapter =
                new PathBuildPageAdapter(this.getSupportFragmentManager(), this);
        this.setPagerAdapter(menuBuildPageAdapter);
    }
}