package com.touriexpert.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.touriexpert.activities.cultura.CultMainActivity;
import com.touriexpert.main.R;
import com.touriexpert.activities.ristorazione.RistMainActivity;
import com.touriexpert.models.UserModel;

public class MainActivity extends Activity {

    private static final String ACTION_FROM_SERVICE = "com.tourismart";
    private static final String W7_PACK = "org.main.wseven";
    private UserModel userModel = UserModel.getInstance();

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        private String sessionCode;
        @Override
        public void onReceive(Context context, Intent intent) {
            this.sessionCode = intent.getExtras().getString("session").equalsIgnoreCase("") ?
                    "Anonymous" : intent.getExtras().getString("session");
            if(this.sessionCode.equals("Anonymous")){
                Toast.makeText(getApplicationContext(), "Loggati su W7!", Toast.LENGTH_SHORT).show();
                startW7App();
            } else {
                userModel.setSessionCode(this.sessionCode);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initilizeW7();
        Button buttonRistorante = (Button) findViewById(R.id.btnTestRist);
        Button buttonCultura = (Button) findViewById(R.id.btnTestCultura);

        buttonRistorante.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RistMainActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });

        buttonCultura.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CultMainActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });
    }

    /**
     * Verifica se il servizio di W7 è attivo
     *
     * @return true se il servizio è attivo, false altrimenti
     */
    public boolean isMainServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (RunningServiceInfo s : manager.getRunningServices(Integer.MAX_VALUE))
            if ("org.main.wseven.MainService".equals(s.service.getClassName()))
                return true;
        return false;
    }

    /**
     * Verifica se un'applicazione W7 è installata a partire dal nome
     * del suo package
     *
     * @param packageName Nome del package da verificare
     * @return true se l'applicazione è installata, false altrimenti
     */
    private boolean appInstalledOrNot(String packageName) {
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    private void initilizeW7(){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        if (appInstalledOrNot(W7_PACK)) {
            System.out.println("W7 is installed");
        } else {
            System.out.println("W7 not installed");
            Toast.makeText(getApplicationContext(), "W7 is not installed!", Toast.LENGTH_SHORT).show();
        }

        if (!isMainServiceRunning()) {
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage(W7_PACK);
            launchIntent.setAction("org.main.wseven.moveTaskToBack");
            startActivity(launchIntent);
        }

        final IntentFilter myFilter = new IntentFilter(ACTION_FROM_SERVICE);
        registerReceiver(mReceiver, myFilter);

        CharSequence text = "";
        Log.d("BroadcastActivity", "Sending message to service: " + text);
        final Intent intent = new Intent("org.main.wseven.POSITION_REQUEST");
        intent.putExtra("data", text);
        intent.putExtra("sender", ACTION_FROM_SERVICE);
        sendBroadcast(intent);
    }

    private void startW7App(){
        PackageManager manager = this.getPackageManager();
        Intent i = manager.getLaunchIntentForPackage("org.main.wseven");
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        this.startActivity(i);
    }
}
