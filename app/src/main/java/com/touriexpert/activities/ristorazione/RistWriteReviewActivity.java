package com.touriexpert.activities.ristorazione;

import android.os.Bundle;

import com.touriexpert.activities.common.TouriSmartActivity;
import com.touriexpert.main.R;

public class RistWriteReviewActivity extends TouriSmartActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initializeView(R.color.white);
    }

    @Override
    protected void initializeView(int mainColor){
        setContentView(R.layout.rist_reviews_write);
        setBackground(mainColor, R.id.mainContainer);
        setToolbar(R.id.toolbar, R.color.white_dark, R.string.rist_write_review_title);
    }
}
