package com.touriexpert.activities.ristorazione;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.touriexpert.main.R;

/**
 * La classe rappresenta l'activity di un singolo menù creato o scelto dalla lista dei
 * menù
 */
public class RistMenuActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rist_menu_main);
        ListView mainList = (ListView)this.findViewById(R.id.menuMainList);
    }
}
