package com.touriexpert.activities.ristorazione;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.MenuItem;
import android.view.View;

import com.touriexpert.activities.common.BuildActivity;
import com.touriexpert.activities.common.NoConnectionActivity;
import com.touriexpert.data.menu.MenuLoader;
import com.touriexpert.main.R;
import com.touriexpert.adapters.menu.MenuBuildPageAdapter;
import com.touriexpert.models.ristorazione.Menu;
import com.touriexpert.models.ristorazione.Articolo;

import java.io.Serializable;
import java.util.List;

/**
 * La classe rappresenta l'activity relativa alla creazione del menù.
 */
public class RistMenuBuildActivity extends BuildActivity
        implements RistDialogFragment.MenuDialogListener, LoaderManager.LoaderCallbacks<Menu>,
            MenuLoader.OnErrorListener {
    private static final int MENUBUILD_LOADER_ID = 1;

    private MenuBuildPageAdapter menuBuildPageAdapter;
    private int mainColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainColor = R.color.rist_red;
        this.initializeView(this.mainColor);
        getSupportLoaderManager().initLoader(MENUBUILD_LOADER_ID, null, this).onContentChanged();
        Intent ownerIntent = getIntent();
        final String owner = ownerIntent.getStringExtra("owner");
        this.setToolbar(R.id.buildToolbar, R.color.rist_red_dark, R.string.rist_menu);
        this.setButtonSave(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Articolo> savedMenu = menuBuildPageAdapter.getAddedItems();
                Intent menuIntent = new Intent(RistMenuBuildActivity.this, RistMenuListActivity.class);
                menuIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                Bundle menuBundle = new Bundle();
                menuBundle.putSerializable("savedMenu", (Serializable)savedMenu);
                menuIntent.putExtra("menuBundle", menuBundle);
                menuIntent.putExtra("owner", owner);
                startActivity(menuIntent);
            }
        });
    }

    @Override
    public void onBackPressed(){
        if(this.menuBuildPageAdapter != null && this.menuBuildPageAdapter.getAddedItems().size() > 0) {
            RistDialogFragment dialog = RistDialogFragment.newInstance(R.string.save_edits,
                    R.string.rist_not_save_menu_question, R.string.go_back);
            dialog.show(this.getSupportFragmentManager(), "RistDialogFragment");
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDialogPositiveClick(RistDialogFragment fragment) {
        super.onBackPressed();
    }

    @Override
    public Loader<Menu> onCreateLoader(int id, Bundle args) {
        MenuLoader loader = new MenuLoader(this);
        loader.setOnErrorListener(this);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Menu> loader, Menu data) {
        this.menuBuildPageAdapter =
                new MenuBuildPageAdapter(getSupportFragmentManager(), data);
        this.setPagerAdapter(menuBuildPageAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Menu> loader) {

    }

    @Override
    public void onLoaderError(Exception error) {
        Intent intent = new Intent(this, NoConnectionActivity.class);
        intent.putExtra("mainColor", this.mainColor);
        this.startActivityForResult(intent, CONNECT_AGAIN_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CONNECT_AGAIN_REQUEST) {
            if (resultCode == Activity.RESULT_OK){
                //this.getLoaderManager().getLoader(MAIN_LOADER_ID).onContentChanged();
                this.getLoaderManager().getLoader(MENUBUILD_LOADER_ID).forceLoad();
            }
        }
    }
}