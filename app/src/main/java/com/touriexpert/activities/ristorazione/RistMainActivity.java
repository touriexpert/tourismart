package com.touriexpert.activities.ristorazione;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.touriexpert.activities.common.MainActivity;
import com.touriexpert.main.R;

public class RistMainActivity extends MainActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initializeView(R.color.rist_red);
        this.setMainButton(R.string.rist_reservation,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(RistMainActivity.this, RistMenuListActivity.class);
                        RistMainActivity.this.startActivity(intent);
                    }
                }
        );
        this.setReviewsButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RistMainActivity.this, RistReviewsListActivity.class);
                RistMainActivity.this.startActivity(intent);
            }
        });
        this.setInfoButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RistMainActivity.this, RistInfoActivity.class);
                RistMainActivity.this.startActivity(intent);
            }
        });
    }
}