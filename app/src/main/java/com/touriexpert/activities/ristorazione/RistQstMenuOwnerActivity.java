package com.touriexpert.activities.ristorazione;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.touriexpert.main.R;

public class RistQstMenuOwnerActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rist_qstmenu_owner);
        final TextInputLayout txtOwnerNameLayout = (TextInputLayout) findViewById(R.id.txtOwnerNameLayout);
        txtOwnerNameLayout.getEditText().setText("Luca");
        Button buttonConfirm = (Button)findViewById(R.id.btnConfirmOwner);
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String owner = txtOwnerNameLayout.getEditText().getText().toString();
                if(owner.length() == 0){
                    txtOwnerNameLayout.setErrorEnabled(true);
                    txtOwnerNameLayout.setError("Non hai inserito il nome!");
                } else {
                    txtOwnerNameLayout.setErrorEnabled(false);
                    Intent intent = new Intent();
                    intent.putExtra("owner", owner);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }

            }
        });
    }
}
