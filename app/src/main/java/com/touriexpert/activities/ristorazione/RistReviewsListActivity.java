package com.touriexpert.activities.ristorazione;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.touriexpert.activities.common.ReviewsActivity;
import com.touriexpert.main.R;


public class RistReviewsListActivity extends ReviewsActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initializeView(R.color.rist_red);
        this.setAddReviewsButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RistReviewsListActivity.this, RistWriteReviewActivity.class);
                RistReviewsListActivity.this.startActivity(intent);
            }
        });
        this.setDefaultAdapter();
    }
}