package com.touriexpert.activities.ristorazione;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.touriexpert.fragments.CounterFragment;
import com.touriexpert.activities.common.TouriSmartActivity;
import com.touriexpert.main.R;
import com.touriexpert.models.ristorazione.ReservationModel;
import com.touriexpert.models.ristorazione.MenuItemModel;
import com.touriexpert.models.ristorazione.RistPayment;
import com.touriexpert.network.RistService;
import com.touriexpert.models.UserModel;

import java.net.ConnectException;
import java.util.List;

public class RistReservationActivity extends TouriSmartActivity {
    private RistService ristService;
    private Integer minCount = 0;
    private Integer maxCount = 0;
    private CounterFragment counter;
    private TextView txtReservTables;
    private TextView txtReservDateTime;
    private List<MenuItemModel> savedMenus;
    private ReservationModel reservationModel = new ReservationModel();
    private UserModel userModel = UserModel.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rist_reservation);
        this.ristService = RistService.getInstance(this);
        this.setToolbar(R.id.rsvToolbar, R.color.rist_red_dark, R.string.reservation_title);

        this.counter = (CounterFragment)this.getSupportFragmentManager()
                .findFragmentById(R.id.peopleCounter);
        this.counter.setCounterTitle(R.string.rist_people_number);
        this.counter.setCounterQuestion(R.string.rist_people_number_qst);

        TextView txtReservTables = (TextView)this.findViewById(R.id.txtReservTables);
        TextView txtReservDateTime = (TextView)this.findViewById(R.id.txtReservDateTime);
        TextView txtAvTables = (TextView)this.findViewById(R.id.txtAvTables);

        Intent intent = getIntent();
        if(intent != null){
            Bundle menuBundle = intent.getBundleExtra("menusBundle");
            this.savedMenus =
                    (List<MenuItemModel>) menuBundle.getSerializable("menus");
            this.minCount = this.savedMenus.size();
            this.maxCount = menuBundle.getInt("tables");
            txtAvTables.setText(this.maxCount.toString());
            this.counter.setMinCount(this.minCount);
            this.counter.setMaxCount(this.maxCount);
            txtReservDateTime.setText(menuBundle.getString("formattedDate") + " alle "
                    + menuBundle.getString("time"));
            if(this.savedMenus.size() > 0){
                txtReservTables.setText(this.savedMenus.size() + " Menù");
            } else {
                txtReservTables.setText("Nessun Menù");
            }
            this.reservationModel.setDate(menuBundle.getString("date"));
            this.reservationModel.setTime(menuBundle.getString("time"));
        }
        Button confirmPay = (Button)this.findViewById(R.id.btnConfirmPay);
        confirmPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TaskMakeReservation().execute();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void initializeView(int mainColor) {

    }

    protected class TaskMakeReservation extends AsyncTask<Void, Void, RistPayment> {

        @Override
        protected RistPayment doInBackground(Void... params) {
            RistPayment ristPayment = null;
            reservationModel.setPeopleMenu(savedMenus.size());
            int peopleNoMenu = counter.getCounterNumber() - savedMenus.size();
            if(peopleNoMenu > 0){
                reservationModel.setPeopleNoMenu(peopleNoMenu);
            }
            try {
               ristPayment = ristService.sendReservation(reservationModel, savedMenus,
                        userModel.getSessionCode());
            } catch (ConnectException e) {
                e.printStackTrace();
            }
            return ristPayment;
        }

        @Override
        protected void onPostExecute(RistPayment payment) {
            Intent intent = new Intent(RistReservationActivity.this, RistPayActivity.class);
            intent.putExtra("ristPayment", payment);
            RistReservationActivity.this.startActivity(intent);
        }
    }
}
