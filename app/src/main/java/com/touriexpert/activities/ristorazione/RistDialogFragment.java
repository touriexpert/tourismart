package com.touriexpert.activities.ristorazione;

import com.touriexpert.main.R;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

public class RistDialogFragment extends DialogFragment {

    interface MenuDialogListener{
        void onDialogPositiveClick(RistDialogFragment fragment);
    }

    MenuDialogListener listener;

    public RistDialogFragment(){}

    public static RistDialogFragment newInstance(int titleID, int messageID, int positiveLabelID){
        RistDialogFragment dialog = new RistDialogFragment();
        Bundle args = new Bundle();
        args.putInt("titleID", titleID);
        args.putInt("messageID", messageID);
        args.putInt("positiveLabelID", positiveLabelID);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int titleID = getArguments().getInt("titleID");
        int messageID = getArguments().getInt("messageID");
        int positiveLabelID = getArguments().getInt("positiveLabelID");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(titleID));
        builder.setMessage(getResources().getString(messageID))
                .setPositiveButton(getResources().getString(positiveLabelID), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onDialogPositiveClick(RistDialogFragment.this);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            this.listener = (MenuDialogListener) activity;
        } catch (ClassCastException e){
            throw new ClassCastException(activity.toString()
                    + " must implement MenuDialogListener");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        int color = ContextCompat.getColor(this.getActivity(), R.color.rist_red);
        ((AlertDialog)getDialog()).getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(color);
        ((AlertDialog)getDialog()).getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(color);
    }
}
