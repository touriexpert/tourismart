package com.touriexpert.activities.ristorazione;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.touriexpert.activities.common.ListActivity;
import com.touriexpert.main.R;
import com.touriexpert.adapters.menu.MenuEditListAdapter;
import com.touriexpert.models.ristorazione.MenuItemModel;
import com.touriexpert.models.ristorazione.Articolo;

import java.io.Serializable;
import java.util.List;

public class RistMenuEditActivity extends ListActivity
        implements MenuEditListAdapter.OnEmptyListListener{
    private MenuEditListAdapter menuAdapter;
    private Button saveButton;

    @Override
    protected void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.rist_menu_edit);
        this.setBackground(R.color.rist_red, R.id.mainContainer);
        this.setToolbar(R.color.rist_red_dark, R.string.rist_edit_menu);
        Intent intent = getIntent();
        Bundle menuBundle = intent.getBundleExtra("menuBundle");
        final MenuItemModel menu =
                (MenuItemModel) menuBundle.getSerializable("menu");
        final int position = menuBundle.getInt("position");
        final List<Articolo> editMenu = menu.getMenu();
        this.menuAdapter = new MenuEditListAdapter(editMenu);
        this.menuAdapter.setOnEmptyListener(this);
        this.setList(this.menuAdapter);
        this.saveButton = (Button) this.findViewById(R.id.btnSave);
        this.saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RistMenuEditActivity.this, RistMenuListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                Bundle menuBundle = new Bundle();
                menuBundle.putSerializable("editMenu", (Serializable) menu);
                menuBundle.putInt("position", position);
                intent.putExtra("editBundle", menuBundle);
                RistMenuEditActivity.this.startActivity(intent);
            }
        });
//        this.setEmptyMessage(R.string.rist_menu_edit_emessage);
//        this.hideEmptyMessage();
    }

    @Override
    protected void initializeView(int mainColor) {

    }

    @Override
    public void onEmptiedList() {
//        this.showEmptyMessage();
    }
}
