package com.touriexpert.activities.ristorazione;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import com.touriexpert.activities.common.TouriSmartActivity;
import com.touriexpert.main.R;
import com.touriexpert.models.PayPalResponse;
import com.touriexpert.models.place.Place;
import com.touriexpert.models.ristorazione.RistPayment;
import com.touriexpert.network.RistService;

import org.json.JSONException;

import java.math.BigDecimal;
import java.net.ConnectException;

public class RistPayActivity extends TouriSmartActivity {
    private Gson gson = new Gson();
    private RistPayment ristPayment;
    private PayPalResponse payPalResponse;
    private LinearLayout mainRsvConfirm;
    private RistService placeService;
    private static PayPalConfiguration config = new PayPalConfiguration()

            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_NO_NETWORK);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.placeService = RistService.getInstance(this);
        setContentView(R.layout.rist_pay);
        this.ristPayment = (RistPayment) this.getIntent().getSerializableExtra("ristPayment");
        if(this.ristPayment != null) {
            boolean flagComanda = false;
            this.mainRsvConfirm = (LinearLayout) this.findViewById(R.id.mainRsvConfirm);
            TextView txtCostoMenu = (TextView)this.findViewById(R.id.txtCostoMenu);
            TextView txtCostoRsv = (TextView)this.findViewById(R.id.txtCostoRsv);
            txtCostoRsv.setText(this.ristPayment.getCostoPrenotazione().toString());
            txtCostoMenu.setText(this.ristPayment.getCostoComanda().toString());

            Place place = null;
            try {
                place = placeService.getPlace();
            } catch (ConnectException e) {
                e.printStackTrace();
            }

            config.clientId(place.getEmail());

            Intent paypalIntent = new Intent(this, PayPalService.class);
            paypalIntent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
            startService(paypalIntent);

            if(this.ristPayment.getCostoComanda() != null){
                flagComanda = true;
            } else {
                txtCostoMenu.setVisibility(View.INVISIBLE);
            }

            PayPalItem[] ppItems = new PayPalItem[2];
            BigDecimal totale = new BigDecimal(0);
            BigDecimal decPrenotazione = new BigDecimal(this.ristPayment.getCostoPrenotazione());
            PayPalItem costoPrenotazione = new PayPalItem("Costo Prenotazione", 1, decPrenotazione,
                    "EUR", "itmPrn");
            ppItems[0] = costoPrenotazione;
            totale = totale.add(decPrenotazione);

            if(flagComanda){
                BigDecimal decComanda = new BigDecimal(this.ristPayment.getCostoComanda());
                PayPalItem costoComanda = new PayPalItem("Costo Comanda", 1, decComanda,
                        "EUR", "itmCmd");
                ppItems[1] = costoComanda;
                totale = totale.add(decComanda);
            }

            String descrizione;
            String dscrPrnComanda = "Prenotazione + Comanda";
            String dscrPrn = "Prenotazione";
            if(flagComanda){
                descrizione = dscrPrnComanda;
            } else {
                descrizione = dscrPrn;
            }

            PayPalPayment payment = new PayPalPayment(totale, "EUR", descrizione,
                    PayPalPayment.PAYMENT_INTENT_SALE);
            payment.items(ppItems);

            Intent intent = new Intent(this, PaymentActivity.class);

            // send the same configuration for restart resiliency
            intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

            intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

            startActivityForResult(intent, 0);
        }

    }

    @Override
    protected void initializeView(int mainColor) {

    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    String payPalJson = confirm.toJSONObject().get("response").toString();
                    this.payPalResponse = this.gson.fromJson(payPalJson, PayPalResponse.class);
                    Log.i("paymentExample", confirm.toJSONObject().toString(4));

                    // TODO: send 'confirm' to your server for verification.
                    // see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                    // for more details.

                    new TaskSendProof(this.placeService).execute();

                } catch (JSONException e) {
                    Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                }
            }
        }
        else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.");
        }
        else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }
    }

    private class TaskSendProof extends AsyncTask<Void, Void, Boolean> {
        private RistService ristService;

        TaskSendProof(RistService ristService){
            this.ristService = ristService;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Boolean isConfirmed = false;
            try {
                isConfirmed = this.ristService.sendPaymentProof(payPalResponse.getId(), ristPayment.getIdComanda(),
                        ristPayment.getIdPrenotazione());
            } catch (ConnectException e) {
                e.printStackTrace();
            }
            return isConfirmed;
        }

        @Override
        protected void onPostExecute(Boolean isConfirmed) {
            return;
        }
    }

    @Override
    public void onBackPressed() {
        NavUtils.navigateUpFromSameTask(this);
        super.onBackPressed();
    }

}
