package com.touriexpert.activities.ristorazione;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.touriexpert.main.R;
import com.touriexpert.activities.common.ListActivity;
import com.touriexpert.adapters.menu.MenuListAdapter;
import com.touriexpert.models.ristorazione.MenuItemModel;
import com.touriexpert.models.ristorazione.ReservationModel;
import com.touriexpert.models.ristorazione.Articolo;
import com.touriexpert.network.RistService;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * La classe rappresenta l'activity relativa alla lista dei menù
 * creati dall'utente.
 */
public class RistMenuListActivity extends ListActivity
        implements TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener,
        RistDialogFragment.MenuDialogListener,
        MenuListAdapter.OnEmptyListListener {

    private static final String TAG = "InfoActivity";
    private static int OWNER_REQUEST = 55;
    private static int MENU_BUILD_REQUEST = 3;
    private MenuListAdapter menuAdapter;
    private RecyclerView mainList;
    private TextView txtTables;
    private TextView txtDateTime;
    private LinearLayout verifyLayout;
    private Button btnVerify;
    private DatePickerDialog dpd;
    private TimePickerDialog tpd;
    private Calendar selectedDate;
    private int selectedYear;
    private int selectedDay;
    private int selectedMonth;
    private int availableTables;
    private RistService ristService;
    private ReservationModel reservModel = new ReservationModel();

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.rist_build_list);
        this.ristService = RistService.getInstance(this);
        this.selectedDate = Calendar.getInstance();
        this.mainList = (RecyclerView) this.findViewById(R.id.mainList);
        this.setToolbar(R.color.rist_red_dark, R.string.rist_menu_list_title);
        this.menuAdapter = new MenuListAdapter(this);
        this.setList(this.menuAdapter);
        this.setSkipButton(R.string.rist_salta_prenota, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RistMenuListActivity.this, RistReservationActivity.class);
                RistMenuListActivity.this.startActivity(intent);
            }
        });
        this.setConfirmButton(R.string.rist_salta_prenota, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reservIntent = new Intent(RistMenuListActivity.this, RistReservationActivity.class);
                Bundle menuBundle = new Bundle();
                menuBundle.putSerializable("menus", (Serializable) menuAdapter.getMenus());
                menuBundle.putInt("tables", availableTables);
                menuBundle.putString("date", reservModel.getDate());
                menuBundle.putString("formattedDate", reservModel.getFormattedDate());
                menuBundle.putString("time", reservModel.getTime());
                reservIntent.putExtra("menusBundle", menuBundle);
                RistMenuListActivity.this.startActivity(reservIntent);
            }
        });
        this.setAddButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RistMenuListActivity.this, RistQstMenuOwnerActivity.class);
                RistMenuListActivity.this.startActivityForResult(intent, OWNER_REQUEST);
            }
        });
        if (this.menuAdapter.getItemCount() > 0) {
            this.hideEmptyMessage();
            this.updateConfirmButton(R.string.rist_conferma_prenota);
        }

        this.txtTables = (TextView) this.findViewById(R.id.txtAvTables);
        this.txtDateTime = (TextView) this.findViewById(R.id.txtDateTime);
        this.verifyLayout = (LinearLayout) this.findViewById(R.id.verifyLayout);
        this.txtTables.setVisibility(View.GONE);
        this.txtDateTime.setVisibility(View.GONE);

        this.btnVerify = (Button) this.findViewById(R.id.btnVerify);
        this.btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                dpd = DatePickerDialog.newInstance(
                        RistMenuListActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Bundle bundle = intent.getBundleExtra("menuBundle");
        Bundle editBundle = intent.getBundleExtra("editBundle");
        if (bundle != null) {
            List<Articolo> savedMenu =
                    (List <Articolo> ) bundle.getSerializable("savedMenu");
            String menuOwner = intent.getStringExtra("owner");
            MenuItemModel menuModel = new MenuItemModel();
            menuModel.setOwner(menuOwner);
            menuModel.setMenu(savedMenu);
            this.menuAdapter.addMenu(menuModel);
            this.menuAdapter.setOnEmptyListener(this);
            this.mainList.scrollToPosition(this.menuAdapter.getItemCount() - 1);
            this.hideEmptyMessage();
            this.updateConfirmButton(R.string.rist_conferma_prenota);
        }
        if(editBundle != null){
            int position = editBundle.getInt("position");
            MenuItemModel editMenu = (MenuItemModel) editBundle.getSerializable("editMenu");
            this.menuAdapter.replaceMenu(editMenu, position);
        }
    }

    @Override
    public void onBackPressed() {
        if (this.menuAdapter.getItemCount() > 0) {
            RistDialogFragment dialog = RistDialogFragment.newInstance(R.string.warning,
                    R.string.rist_menu_list_question, R.string.go_back);
            dialog.show(this.getSupportFragmentManager(), "RistDialogFragment");
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onDialogPositiveClick(RistDialogFragment fragment) {
        super.onBackPressed();
    }

    @Override
    public void onEmptiedList() {
        this.showEmptyMessage();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        this.selectedYear = year;
        this.selectedDay = dayOfMonth;
        this.selectedMonth = monthOfYear;
        Calendar now = Calendar.getInstance();
        tpd = TimePickerDialog.newInstance(
                RistMenuListActivity.this,
                now.get(Calendar.HOUR),
                now.get(Calendar.MINUTE),
                true
        );
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        this.selectedDate.set(this.selectedYear, this.selectedMonth,
                this.selectedDay, hourOfDay, minute);
        new TaskFetchTables().execute();
    }

    protected class TaskFetchTables extends AsyncTask< Void, Void, Integer > {

        @Override
        protected Integer doInBackground(Void...voids) {
            setReservationModel(reservModel);
            return ristService.getRistTables();
        }

        @Override
        protected void onPostExecute(Integer tables) {
            txtDateTime.setVisibility(View.VISIBLE);
            txtTables.setVisibility(View.VISIBLE);
            availableTables = tables;
            txtTables.setText("Sono disponibili " + tables + " posti");
            txtDateTime.setText(reservModel.getFormattedDate() + " alle " + reservModel.getTime());
            btnVerify.setText(R.string.change_verify_date);
            showAddButton();
        }
    }

    private void setReservationModel(ReservationModel model) {
        SimpleDateFormat formattedFormat = new SimpleDateFormat("EEEE dd MMMM");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        model.setFormattedDate(formattedFormat.format(this.selectedDate.getTime()));
        model.setTime(timeFormat.format(this.selectedDate.getTime()));
        model.setDate(dateFormat.format(this.selectedDate.getTime()));
    }

    protected void onPause(){
        super.onPause();
        Log.d(TAG, "RistMenuListActivity onPause");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == OWNER_REQUEST) {
            if(resultCode == Activity.RESULT_OK) {
                String owner = data.getStringExtra("owner");
                Intent intent = new Intent(this, RistMenuBuildActivity.class);
                intent.putExtra("owner", owner);
                this.startActivityForResult(intent, MENU_BUILD_REQUEST);
            }
        }
    }
}