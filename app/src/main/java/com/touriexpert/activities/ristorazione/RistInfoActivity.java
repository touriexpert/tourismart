package com.touriexpert.activities.ristorazione;

import android.os.Bundle;

import com.touriexpert.main.R;
import com.touriexpert.activities.common.InfoActivity;

public class RistInfoActivity extends InfoActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initializeView(R.color.rist_red);
    }
}
