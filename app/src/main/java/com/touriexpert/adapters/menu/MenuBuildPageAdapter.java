package com.touriexpert.adapters.menu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.touriexpert.fragments.BuildFragment;
import com.touriexpert.models.ristorazione.Articoli;
import com.touriexpert.models.ristorazione.Menu;
import com.touriexpert.models.ristorazione.Articolo;

import java.util.ArrayList;
import java.util.List;

public class MenuBuildPageAdapter extends FragmentPagerAdapter {
    private final FragmentManager fragmentManager;
    private Menu mainMenu;
    private final List<MenuBuildListAdapter> adapters;
    private List<String> categorie;

    public MenuBuildPageAdapter(FragmentManager fm, Menu menu){
        super(fm);
        this.fragmentManager = fm;
        this.adapters = new ArrayList<>();
        this.mainMenu = menu;
        this.categorie = this.mainMenu.getCategorie();
    }

    @Override
    public Fragment getItem(int position) {
        String categoria = this.categorie.get(position);
        Articoli articoli = this.mainMenu.getArticoli();
        MenuBuildListAdapter adapter = new MenuBuildListAdapter(articoli.getArticoli(categoria));
        this.adapters.add(adapter);
        BuildFragment buildFragment = new BuildFragment();
        Bundle fragmentBundle = new Bundle();
        fragmentBundle.putSerializable("menuAdapter", adapter);
        buildFragment.setArguments(fragmentBundle);
        return buildFragment;
    }

    @Override
    public int getCount() {
        return categorie.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return categorie.get(position).toUpperCase();
    }

    public List<Articolo> getAddedItems(){
        List<Articolo> menu = new ArrayList<Articolo>();
        for(MenuBuildListAdapter adapter : this.adapters){
            menu.addAll(adapter.getAddedItems());
        }
        return menu;
    }
}
