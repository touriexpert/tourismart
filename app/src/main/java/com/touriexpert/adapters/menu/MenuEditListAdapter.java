package com.touriexpert.adapters.menu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.touriexpert.main.R;
import com.touriexpert.models.ristorazione.Articolo;

import java.util.List;

public class MenuEditListAdapter extends RecyclerView.Adapter<MenuEditListAdapter.BuildItemHolder> {
    private List<Articolo> addedItems;
    private OnEmptyListListener listener;

    public interface OnEmptyListListener{
        void onEmptiedList();
    }

    class BuildItemHolder extends RecyclerView.ViewHolder {
        TextView txtItemName, txtItemDescription, txtItemPrice;
        Button btnAddItem, btnDeleteItem;

        BuildItemHolder(View view) {
            super(view);
            this.txtItemName = (TextView) view.findViewById(R.id.txtItemName);
            this.txtItemDescription = (TextView) view.findViewById(R.id.txtItemDescription);
            this.txtItemPrice = (TextView) view.findViewById(R.id.txtItemPrice);
            this.btnAddItem = (Button) view.findViewById(R.id.btnAddItem);
            this.btnDeleteItem = (Button) view.findViewById(R.id.btnDeleteItem);
        }
    }


    public MenuEditListAdapter(List<Articolo> addedItems) {
        this.addedItems = addedItems;
    }

    @Override
    public BuildItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rist_build_list_item, parent, false);
        return new BuildItemHolder(rowView);
    }

    @Override
    public void onBindViewHolder(BuildItemHolder holder, int position) {
        int pos = position;
        final Articolo menuModel = this.addedItems.get(position);
        final BuildItemHolder itemHolder = holder;
        holder.btnAddItem.setVisibility(View.GONE);
        holder.txtItemName.setText(menuModel.getNome());
        holder.txtItemPrice.setText(menuModel.getPrezzo().toString());
        holder.txtItemDescription.setText(menuModel.getDescrizione());
        holder.btnDeleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addedItems.remove(menuModel);
                notifyDataSetChanged();
                if(addedItems.size() == 0){
                    listener.onEmptiedList();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.addedItems.size();
    }

    public List<Articolo> getAddedItems(){
        return addedItems;
    }

    public void setAddedItems(List<Articolo> items){
        this.addedItems = items;
    }

    public void setOnEmptyListener(Context context){
        try{
            this.listener = (OnEmptyListListener) context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString()
                    + " must implement OnEmptyListListener");
        }
    }

}