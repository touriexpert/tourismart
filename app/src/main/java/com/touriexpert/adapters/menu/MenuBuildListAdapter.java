package com.touriexpert.adapters.menu;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.touriexpert.main.R;
import com.touriexpert.models.ristorazione.Articolo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MenuBuildListAdapter
        extends RecyclerView.Adapter<MenuBuildListAdapter.BuildItemHolder> implements Serializable {
    private List<Articolo> menuList;
    private List<Articolo> addedItems;
    private Map<Integer, Integer> originalIndexes;
    private ItemAddedListener itemAddedListener;

    public interface ItemAddedListener {
        void onItemAdded();
    }

    public void setItemAddedListener(ItemAddedListener itemAddedListener) {
        this.itemAddedListener = itemAddedListener;
    }

    static class BuildItemHolder extends RecyclerView.ViewHolder {
        TextView txtItemName, txtItemDescription, txtItemPrice;
        Button btnAddItem, btnDeleteItem;

        BuildItemHolder(View view) {
            super(view);
            this.txtItemName = (TextView) view.findViewById(R.id.txtItemName);
            this.txtItemDescription = (TextView) view.findViewById(R.id.txtItemDescription);
            this.txtItemPrice = (TextView) view.findViewById(R.id.txtItemPrice);
            this.btnAddItem = (Button) view.findViewById(R.id.btnAddItem);
            this.btnDeleteItem = (Button) view.findViewById(R.id.btnDeleteItem);
            this.btnDeleteItem.setVisibility(View.INVISIBLE);
        }
    }

    public MenuBuildListAdapter(List<Articolo> menu) {
        this.menuList = menu;
        this.originalIndexes = new HashMap<>();
        this.addedItems = new ArrayList<>();
    }

    @Override
    public BuildItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rist_build_list_item, parent, false);
        return new BuildItemHolder(rowView);
    }

    @Override
    public void onBindViewHolder(BuildItemHolder holder, int position) {
        final Articolo articolo = this.menuList.get(position);
        final BuildItemHolder itemHolder = holder;
        final int pos = position;
        holder.txtItemName.setText(articolo.getNome());
        holder.txtItemPrice.setText(articolo.getPrezzo().toString());
        holder.txtItemDescription.setText(articolo.getDescrizione());
        holder.btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuList.add(0, menuList.remove(pos));
                itemHolder.btnAddItem.setVisibility(View.INVISIBLE);
                itemHolder.btnDeleteItem.setVisibility(View.VISIBLE);
                notifyItemRemoved(pos);
                notifyItemRangeChanged(pos, menuList.size());
                itemAddedListener.onItemAdded();
            }
        });

        holder.btnDeleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = originalIndexes.get(articolo.getId());
                addedItems.remove(articolo);
                menuList.add(position, menuList.remove(pos));
                itemHolder.btnDeleteItem.setVisibility(View.INVISIBLE);
                itemHolder.btnAddItem.setVisibility(View.VISIBLE);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.menuList.size();
    }

    public List<Articolo> getAddedItems(){
        return addedItems;
    }

}