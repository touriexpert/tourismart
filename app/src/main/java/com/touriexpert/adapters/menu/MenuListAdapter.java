package com.touriexpert.adapters.menu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.touriexpert.main.R;
import com.touriexpert.activities.ristorazione.RistMenuEditActivity;
import com.touriexpert.models.ristorazione.MenuItemModel;

import java.util.ArrayList;
import java.util.List;

public class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.MenuItemHolder> {
    private List<MenuItemModel> menuList;
    private Context context;
    OnEmptyListListener listener;


    public interface OnEmptyListListener{
        void onEmptiedList();
    }

    class MenuItemHolder extends RecyclerView.ViewHolder {
        TextView txtMenuOwnerName, txtMenuPrice;
        Button btnDeleteMenu;

        MenuItemHolder(View view) {
            super(view);
            this.txtMenuOwnerName = (TextView) view.findViewById(R.id.txtMenuOwnerName);
            this.txtMenuPrice = (TextView) view.findViewById(R.id.txtMenuPrice);
            this.btnDeleteMenu = (Button) view.findViewById(R.id.btnDeleteMenu);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int adPosition = getAdapterPosition();
                    Intent intent = new Intent(context, RistMenuEditActivity.class);
                    Bundle menuBundle = new Bundle();
                    menuBundle.putSerializable("menu", menuList.get(adPosition));
                    menuBundle.putInt("position", adPosition);
                    intent.putExtra("menuBundle", menuBundle);
                    context.startActivity(intent);
                }
            });
        }
    }

    public MenuListAdapter(Context c) {
        this.context = c;
        this.menuList = new ArrayList<>();
    }

    public void setOnEmptyListener(Context context){
        try{
            this.listener = (OnEmptyListListener) context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString()
                    + " must implement OnEmptyListListener");
        }
    }

    @Override
    public MenuItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View rowView = inflater.inflate(R.layout.rist_menu_list_item, parent, false);
        return new MenuItemHolder(rowView);
    }

    @Override
    public void onBindViewHolder(MenuItemHolder holder, int position) {
        final int rPosition = position;
        MenuItemModel menuItem = this.menuList.get(position);
        holder.txtMenuOwnerName.setText(menuItem.getOwner());
        holder.txtMenuPrice.setText(menuItem.getPrice().toString());
        holder.btnDeleteMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuItemModel item = menuList.get(rPosition);
                menuList.remove(item);
                notifyDataSetChanged();
                if(menuList.size() == 0){
                    listener.onEmptiedList();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }

    public void addMenu(MenuItemModel item){
        this.menuList.add(item);
        this.notifyDataSetChanged();
    }

    public void replaceMenu(MenuItemModel item, int position){
        this.menuList.set(position, item);
        this.notifyDataSetChanged();
    }

    public List<MenuItemModel> getMenus(){
        return this.menuList;
    }
}
