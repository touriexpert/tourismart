package com.touriexpert.models.place;

import java.util.Map;

public class Place {

    class Indirizzo {
        String citta;
        String via;
        String cap;
        String civico;

        public String getCitta() {
            return citta;
        }

        public void setCitta(String citta) {
            this.citta = citta;
        }

        public String getVia() {
            return via;
        }

        public void setVia(String via) {
            this.via = via;
        }

        public String getCap() {
            return cap;
        }

        public void setCap(String cap) {
            this.cap = cap;
        }

        public String getCivico() {
            return civico;
        }

        public void setCivico(String civico) {
            this.civico = civico;
        }

        @Override
        public String toString(){
            return this.getVia() + ", " + this.getCivico();
        }
    }
    private Integer id;
    private String website;
    private Indirizzo indirizzo;
    private String nome;
    private String telefono;

    public void setIndirizzo(Indirizzo indirizzo) {
        this.indirizzo = indirizzo;
    }

    private String email;
    private Map<String, String[]> orari;


    public String getWebsite() {
        return website;
    }


    public void setWebsite(String website) {
        this.website = website;
    }


    public String getIndirizzo() {
        return indirizzo.toString();
    }


    public String getNome() {
        return nome;
    }


    public void setNome(String nome) {
        this.nome = nome;
    }


    public String getTelefono() {
        return telefono;
    }


    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }


    public Map<String, String[]> getOrari() {
        return orari;
    }


    public void setOrari(Map<String, String[]> orari) {
        this.orari = orari;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public Integer getId() {
        return id;
    }

    void setId(Integer id) {
        this.id = id;
    }
}
