package com.touriexpert.models;

public class UserModel {

    private String sessionCode;
    private static UserModel singleton;

    private UserModel() {
        super();
    }

    public static UserModel getInstance(){
        if(singleton == null) {
            singleton = new UserModel();
        }
        return singleton;
    }

    public String getSessionCode() {
        return sessionCode;
    }

    public void setSessionCode(String sessionCode) {
        this.sessionCode = sessionCode;
    }

}
