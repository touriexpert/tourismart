package com.touriexpert.models.ristorazione;

public class ReservationModel {
    private int peopleMenu;
    private int peopleNoMenu;
    private String formattedDate;
    private String date;
    private String time;

    public int getPeopleMenu() {
        return peopleMenu;
    }

    public void setPeopleMenu(int peopleMenu) {
        this.peopleMenu = peopleMenu;
    }

    public String getFormattedDate() {
        return formattedDate;
    }

    public void setFormattedDate(String date) {
        this.formattedDate = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return this.date;
    }

    public int getPeopleNoMenu() {
        return this.peopleNoMenu;
    }

    public void setPeopleNoMenu(int peopleNoMenu) {
        this.peopleNoMenu = peopleNoMenu;
    }
}