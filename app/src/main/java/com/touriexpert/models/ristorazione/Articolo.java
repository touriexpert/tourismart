package com.touriexpert.models.ristorazione;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

public class Articolo implements Serializable {
    private int id;
    private Double prezzo;
    private String nome;
    private String descrizione;
    private List<String> categorie;

    public Articolo() {
        this.id = -1;
        this.prezzo = 0.0;
        this.nome = "";
        this.descrizione = "";
        this.categorie = new ArrayList<>();
    }

    public Double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(Double prezzo) {
        this.prezzo = prezzo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getCategorie() {
        return categorie;
    }

    public void setCategorie(List<String> categorie) {
        this.categorie = categorie;
    }

    public void addCategoria(String categoria){
        this.categorie.add(categoria);
    }
}
