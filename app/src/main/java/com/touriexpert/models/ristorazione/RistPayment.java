package com.touriexpert.models.ristorazione;

import java.io.Serializable;

public class RistPayment implements Serializable{
    Integer idPrenotazione;
    Integer idComanda;
    Double costoPrenotazione;
    Double costoComanda;

    public Integer getIdPrenotazione() {
        return idPrenotazione;
    }

    public void setIdPrenotazione(Integer idPrenotazione) {
        this.idPrenotazione = idPrenotazione;
    }

    public Integer getIdComanda() {
        return idComanda;
    }

    public void setIdComanda(Integer idComanda) {
        this.idComanda = idComanda;
    }

    public Double getCostoPrenotazione() {
        return costoPrenotazione;
    }

    public void setCostoPrenotazione(Double costoPrenotazione) {
        this.costoPrenotazione = costoPrenotazione;
    }

    public Double getCostoComanda() {
        return costoComanda;
    }

    public void setCostoComanda(Double costoComanda) {
        this.costoComanda = costoComanda;
    }

    @Override
    public String toString() {
        return "RistPayment{" +
                "idPrenotazione='" + this.idPrenotazione + '\'' +
                ", idComanda='" + this.idComanda + '\'' +
                ", costoPrenotazione=" + this.costoPrenotazione +
                ", costoComanda=" + this.costoComanda +
                '}';
    }
}
