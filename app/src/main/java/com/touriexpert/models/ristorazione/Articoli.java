package com.touriexpert.models.ristorazione;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Articoli {
    private Map<String, List<Articolo>> articoliMenu;

    Articoli(){
        this.articoliMenu = new HashMap<>();
    }

    public void addArticolo(String categoria, Articolo art){
        if(this.articoliMenu.get(categoria) == null){
            ArrayList<Articolo> listArticolo = new ArrayList<Articolo>();
            listArticolo.add(art);
            this.articoliMenu.put(categoria, listArticolo);
        } else {
            List<Articolo> listArticolo = this.articoliMenu.get(categoria);
            listArticolo.add(art);
        }
    }

    public List<Articolo> getArticoli(String categoria) {
        return this.articoliMenu.get(categoria);
    }
}
