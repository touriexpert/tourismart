package com.touriexpert.models.ristorazione;

import java.util.List;

public class Menu {

    private int id;

    private List<String> categorie;

    private Articoli articoli;

    public Menu(){
        articoli = new Articoli();
    }

    public Articoli getArticoli() {
        return this.articoli;
    }

    public void setArticoli(Articoli articoli) {
        this.articoli = articoli;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getCategorie() {
        return categorie;
    }

    public void setCategorie(List<String> categorie) {
        this.categorie = categorie;
    }


}
