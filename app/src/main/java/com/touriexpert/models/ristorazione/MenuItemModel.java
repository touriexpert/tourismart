package com.touriexpert.models.ristorazione;

import java.io.Serializable;
import java.util.List;

public class MenuItemModel implements Serializable {
    private String owner;
    private Double price;
    private List<Articolo> menu;

    public MenuItemModel(){
        this.price = 0.0;
        this.owner = "";
    }

    public MenuItemModel(List<Articolo> menu){
        this.menu = menu;
        this.price = calculatePrice();
    }

    private Double calculatePrice(){
        Double price = 0.0;
        for(Articolo item : this.menu){
            price += item.getPrezzo();
        }
        return price;
    }

    public void setMenu(List<Articolo> menu) {
        this.menu = menu;
        this.price = calculatePrice();
    }

    public List<Articolo> getMenu() {
        return menu;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
